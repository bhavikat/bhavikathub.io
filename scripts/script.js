
var resumeApp = angular.module('resumeApp', ["ngRoute"]);

 resumeApp.config(function($routeProvider)
 {
        $routeProvider.when('/',
            {
                templateUrl: "/me.html",
                controller : "mainController"
            })
            .when('/skills',
            {
                templateUrl:"/skills.html",
                controller: "skillsController"
            })
            .when('/contact',
            {
                templateUrl: "/contact.html",
                controller: "contactController"
            })
            .when('/work',
            {
                templateUrl: "/work.html",
                controller: "workController"
            })
          
            .otherwise({redirectTo: '/'});
});

