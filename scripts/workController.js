(function () {
    'use strict';

    var resumeApp = angular.module('resumeApp');
    resumeApp.controller('workController', ['$scope', function ($scope) {

        
        $scope.dataminingreport = "Comparison of K-Means, DBSCAN and DBKMeans clustering algorithms in relation to spatial databases";
        $scope.javacert = "Oracle Certified Java Professional"
        $scope.beproject = "Project Synopsis: Text Illustration Using Information Extraction"
        $scope.coapresentation = "Presentation: SPARC T1"
        $scope.angjsPS = "Pluralsight - AngularJS : Getting Started";
        $scope.bowerPS = "Pluralsight - Bower Fundamentals";
        $scope.mysqlPS = "Pluralsight - MySQL Query Perfomance and Fine tuning";
        $scope.agilePS = "Pluralsight - Agile Fundamentals";
        $scope.courseradatascience = "Certificate: The Data Scientist's Toolbox by John Hopkins University (Coursera)";
        $scope.aboutCourseraDS = "Course details: Introduction to R, Git and Github";
        
        $scope.iycollege = "Admissions web application for IY College, Mumbai"
        $scope.hadoop = "Presentation: Hadoop"
        
        
        
    }]);

}());