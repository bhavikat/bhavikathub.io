(function () {
    'use strict';

    var resumeApp = angular.module('resumeApp');
    resumeApp.controller('skillsController', ['$scope', function ($scope) {

        
        $scope.skillset = "Java, C#, Python";
        $scope.ml = "Machine learning and data science applications, web scraping"
        $scope.webdev = "HTML, CSS, JavaScript"
        
    }]);

}());